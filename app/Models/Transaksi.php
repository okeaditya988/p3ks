<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Transaksi
 *
 * @property int $m_t_id
 * @property Carbon $m_t_tanggal
 * @property float $m_t_nominal
 * @property string|null $m_t_deskripsi
 * @property int $m_t_m_k_id
 * @property Carbon $m_t_created_at
 * @property Carbon|null $m_t_updated_at
 * @property string|null $deleted_at
 *
 * @property Kategori $kategori
 *
 * @package App\Models
 */
class Transaksi extends Model
{
	use SoftDeletes;
	protected $table = 'transaksi';
	protected $primaryKey = 'm_t_id';
	public $timestamps = false;

	protected $casts = [
		'm_t_nominal' => 'float',
		'm_t_m_k_id' => 'int'
	];

	protected $dates = [
		'm_t_created_at',
		'm_t_updated_at',
        'deleted_at'
	];

	protected $fillable = [
		'm_t_tanggal',
		'm_t_nominal',
		'm_t_deskripsi',
		'm_t_m_k_id',
		'm_t_created_at',
		'm_t_updated_at'
	];

	public function kategori()
	{
		return $this->belongsTo(Kategori::class, 'm_t_m_k_id');
	}
}
