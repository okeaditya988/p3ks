<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Kategori
 *
 * @property int $m_k_id
 * @property string $m_k_nama
 * @property string|null $m_k_deskripsi
 * @property string $m_k_jenis
 * @property Carbon $m_k_created_at
 * @property Carbon|null $m_k_updated_at
 * @property string|null $deleted_at
 *
 * @property Collection|Transaksi[] $transaksis
 *
 * @package App\Models
 */
class Kategori extends Model
{
	use SoftDeletes;
	protected $table = 'kategori';
	protected $primaryKey = 'm_k_id';
	public $timestamps = false;

	protected $dates = [
		'm_k_created_at',
        'm_k_updated_at',
        'deleted_at'
	];

	protected $fillable = [
		'm_k_nama',
		'm_k_deskripsi',
		'm_k_jenis',
		'm_k_created_at',
		'm_k_updated_at'
	];

	public function transaksis()
	{
		return $this->hasMany(Transaksi::class, 'm_t_m_k_id');
	}
}
