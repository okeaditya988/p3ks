@extends('layouts.templates')
@section('content')

<div class="block block-themed">
    <div class="block-header block-header-default bg-primary-danger">

        <div class="col-md-2">
            <h3 class="block-title">Data Transaksi</h3>
        </div>
        <div class="col-md-2">
            <h3 class="block-title">Pilih Tanggal</h3>
        </div>
        <div class="col-md-2">
            <input class="jsFlatpickr" type="text" placeholder="Select Date.." data-id="range" readonly="readonly">
        </div>
        <div class="col-md-4 text-right">
            <h3 class="block-title">Sisa Saldo : Rp. {{number_format($data->sisaSaldo->sisaSaldo,0, ',' , '.')}}</h3>
        </div>
        <div class="col-md-2">
            <a type="button" class="btn btn-success" href="/transaksi/tambah">
                <i class="fa fa-plus"></i>
                Tambah
            </a>
        </div>
    </div>
    <div class="block-content block-content-full">
        <table id="dataTable" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Tanggal</th>
                    <th>Jenis Transaksi</th>
                    <th>Kategori</th>
                    <th>Nominal</th>
                    <th>Deskripsi</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody id="show_data">
                @foreach($data->tampil as $t)
                <tr>
                    <td>{{$data->i++}}</td>
                    <td>{{date('d F Y', strtotime($t->m_t_tanggal))}}</td>
                    <td>{{$t->kategori->m_k_jenis}}</td>
                    <td>{{$t->kategori->m_k_nama}}</td>
                    <td>Rp. {{number_format($t->m_t_nominal,0, ',' , '.')}}</td>
                    <td>{{$t->m_t_deskripsi}}</td>
                    <td>
                        <a class="btn btn-sm btn-warning mr-2 mb-2 button_ubah" onclick="tampil_modal('{{$t->m_t_id}}','ubah')"><i class="fa fa-edit"></i> Ubah</a>
                        <a class="btn btn-sm btn-danger mr-2 mb-2 button_hapus" onclick="tampil_modal('{{$t->m_t_id}}','hapus')"><i class="fa fa-trash"></i> Hapus</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade modal_tampil" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="myModalLabel"></h3>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            </div>
            <form class="form-horizontal form" method="post" autocomplete="off">
                {{ csrf_field() }}
                <div class="modal-body row">
                    <input name="m_t_id" class="form-control m_t_id" type="hidden">
                    <div class="form-group col-sm-12 row">
                        <label class="col-sm-3 col-form-label">Jenis Transaksi*</label>
                        <div class="col-sm-9">
                            <select name="jenis" class="form-control jenis" required>
                                <option value="">-Pilih-</option>
                                <option value="Pemasukan">Pemasukan</option>
                                <option value="Pengeluaran">Pengeluaran</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-sm-12 row">
                        <label class="col-sm-3 col-form-label">Tanggal*</label>
                        <div class="col-sm-9">
                            <input type="text" name="m_t_tanggal" class="form-control m_t_tanggal" required>
                        </div>
                    </div>
                    <div class="form-group col-sm-12 row">
                        <label class="col-sm-3 col-form-label">Kategori*</label>
                        <div class="col-sm-9">
                            <select name="m_t_m_k_id" class="form-control m_t_m_k_id" required>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-sm-12 row">
                        <label class="col-sm-3 col-form-label">Nominal</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        Rp
                                    </span>
                                </div>
                                <input name="m_t_nominal" class="form-control m_t_nominal" type="text" onkeypress="return hanyaAngka(event)">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-sm-12 row">
                        <label class="col-sm-3 col-form-label">Deskripsi</label>
                        <div class="col-sm-9">
                            <textarea name="m_t_deskripsi" class="form-control m_t_deskripsi"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button class="btn btn-info simpan">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

@stop

@section('js')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(".m_t_tanggal").flatpickr({
        dateFormat: "Y-m-d"
    });

    var date = new Date();
    $(".jsFlatpickr").flatpickr({
        mode: "range",
        dateFormat: "Y-m-d",
        altFormat: "Y-m-d",
        defaultDate: "today",
        defaultDate: [new Date(date.getFullYear(), date.getMonth(), 1),new Date(date.getFullYear(), date.getMonth() + 1, 0)],
        onClose: function(selectedDates, dateStr, instance){
            var tanggal = $(".jsFlatpickr").val().split(" to ");
            $.ajax({
                url   : "{{route('transaksi.tanggal')}}",
                type : 'POST',
                data : {
                    tanggal:tanggal
                },
                dataType : 'json',
                success : function(data){
                    var html = '';
                    var i = 1;
                    data.forEach(function(key){
                        if(key.m_t_deskripsi==null){
                            key.m_t_deskripsi='';
                        }
                        html += '<tr>'+
                                '<td>'+i+'</td>'+
                                '<td>'+key.m_t_tanggal+'</td>'+
                                '<td>'+key.kategori.m_k_jenis+'</td>'+
                                '<td>'+key.kategori.m_k_nama+'</td>'+
                                '<td>'+key.m_t_nominal+'</td>'+
                                '<td>'+key.m_t_deskripsi+'</td>'+
                                '<td>'+
                                '<a class="btn btn-sm btn-warning mr-2 mb-2 button_ubah" onclick='+"tampil_modal('"+key.m_t_id+"','ubah')"+'><i class="fa fa-edit"></i> Ubah</a>'+
                                '<a class="btn btn-sm btn-danger mr-2 mb-2 button_hapus" onclick='+"tampil_modal('"+key.m_t_id+"','hapus')"+'><i class="fa fa-trash"></i> Hapus</a>'+
                                '</td>'+
                                '</tr>';
                        i++;
                    });
                    $('#show_data').html(html);
                }
            });
        }
    });


    $(".jenis").on('change', function(){
        $(".m_t_m_k_id").val('');
        $(".m_t_nominal").val('');
        $(".m_t_deskripsi").val('');
        var id = $(this).children("option:selected").val();
        $.ajax({
                url: "{{route('transaksi.getKategori')}}",
                type: "POST",
                data: {
                    id: id
                },
                dataType: 'json',
                success: function(respond) {
                    var option = '<option value="">-Pilih-</option>';
                    respond.forEach(function(key){
                        option += '<option value="'+key.m_k_id+'">'+key.m_k_nama+'</option>';
                    });
                    $(".m_t_m_k_id").html(option);
                },
                error: function() {
                    console.log('Error');
                }
            });
    });

    $(".m_t_nominal").on('keyup', function(){
        var sisaSaldo = {{$data->sisaSaldo->sisaSaldo}};
        var nominal = $(this).val();
        var jenis = $(".jenis").children("option:selected").val();
        if(nominal>sisaSaldo && jenis=='Pengeluaran'){
            $(this).val('');
            $.notify({
                title: '<strong>Perhatian!</strong>',
                message: 'Nominal Pengeluaran Lebih Besar Dari Saldo'
            },{
                type: 'danger'
            });
        }
        if(nominal==0){
            $(".m_t_nominal").val('');
        }
    });

    function tampil_modal(id,jenis){
        if(jenis=='ubah'){
            $("#myModalLabel").html('Ubah Transaksi');
            $(".form").attr("action", "{{route('transaksi.update')}}");
            $.ajax({
                url: "{{route('transaksi.get')}}",
                type: "POST",
                data: {
                    id: id
                },
                dataType: 'json',
                success: function(respond) {
                    var option = '<option value="">-Pilih-</option>';
                    respond.kategori.forEach(function(key){
                        option += '<option value="'+key.m_k_id+'">'+key.m_k_nama+'</option>';
                    });
                    $(".m_t_m_k_id").html(option);
                    $(".m_t_id").val(respond.tampil.m_t_id);
                    $(".m_t_tanggal").val(respond.tampil.m_t_tanggal);
                    $(".jenis").val(respond.tampil.kategori.m_k_jenis);
                    $(".m_t_m_k_id").val(respond.tampil.m_t_m_k_id);
                    $(".m_t_nominal").val(respond.tampil.m_t_nominal);
                    $(".m_t_deskripsi").val(respond.tampil.m_t_deskripsi);
                },
                error: function() {
                    console.log('Error');
                }
            });
            $(".modal_tampil").modal('show');
        }else{
            tombol_hapus(id,"{{route('transaksi.destroy')}}");
        }
    }

</script>

@endsection
