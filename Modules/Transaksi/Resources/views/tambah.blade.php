@extends('layouts.templates')
@section('content')

<div class="block block-themed">
    <div class="block-header block-header-default bg-primary-danger">

        <div class="col-md-4">
            <h3 class="block-title">Tambah Transaksi</h3>
        </div>
        <div class="col-md-8 text-right">
            <h3 class="block-title">Sisa Saldo : Rp. {{number_format($data->sisaSaldo,0, ',' , '.')}}</h3>
        </div>
    </div>
    <div class="block-content block-content-full">
        <form class="form-horizontal form" method="post" action="{{route('transaksi.store')}}" autocomplete="off">
            {{ csrf_field() }}
            <div class="modal-body row">
                <div class="form-group col-sm-12 row">
                    <label class="col-sm-3 col-form-label">Jenis Transaksi*</label>
                    <div class="col-sm-9">
                        <select name="jenis" class="form-control jenis" required>
                            <option value="">-Pilih-</option>
                            <option value="Pemasukan">Pemasukan</option>
                            <option value="Pengeluaran">Pengeluaran</option>
                        </select>
                    </div>
                </div>
                <div class="form-group col-sm-12 row">
                    <label class="col-sm-3 col-form-label">Tanggal*</label>
                    <div class="col-sm-9">
                        <input type="text" name="m_t_tanggal" class="form-control m_t_tanggal" required>
                    </div>
                </div>
                <div class="form-group col-sm-12 row">
                    <label class="col-sm-3 col-form-label">Kategori*</label>
                    <div class="col-sm-9">
                        <select name="m_t_m_k_id" class="form-control m_t_m_k_id" required>
                        </select>
                    </div>
                </div>
                <div class="form-group col-sm-12 row">
                    <label class="col-sm-3 col-form-label">Nominal</label>
                    <div class="col-sm-9">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    Rp
                                </span>
                            </div>
                            <input name="m_t_nominal" class="form-control m_t_nominal" type="text" onkeypress="return hanyaAngka(event)">
                        </div>
                    </div>
                </div>
                <div class="form-group col-sm-12 row">
                    <label class="col-sm-3 col-form-label">Deskripsi</label>
                    <div class="col-sm-9">
                        <textarea name="m_t_deskripsi" class="form-control m_t_deskripsi"></textarea>
                    </div>
                </div>
                <div class="col-md-12 text-right">
                    <button class="btn btn-lg btn-info simpan">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>

@stop

@section('js')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function(){
        hapus('disabled',true);
    });

    $(".m_t_tanggal").flatpickr({
        dateFormat: "Y-m-d",
        defaultDate: "today",
    });

    $(".jenis").on('change', function(){
        $(".m_t_m_k_id").val('');
        $(".m_t_nominal").val('');
        $(".m_t_deskripsi").val('');
        var jenis = $(this).children("option:selected").val();
        if(jenis){
            hapus('disabled',false);
        }else{
            hapus('disabled',true);
        }
    });

    function hapus(jenis,value){
        $(".m_t_m_k_id").attr(jenis,value);
        $(".m_t_nominal").attr(jenis,value);
        $(".m_t_deskripsi").attr(jenis,value);
    }

    $(".m_t_nominal").on('keyup', function(){
        var sisaSaldo = {{$data->sisaSaldo}};
        var nominal = $(this).val();
        var jenis = $(".jenis").children("option:selected").val();
        if(nominal>sisaSaldo && jenis=='Pengeluaran'){
            $(this).val('');
            $.notify({
                title: '<strong>Perhatian!</strong>',
                message: 'Nominal Pengeluaran Lebih Besar Dari Saldo'
            },{
                type: 'danger'
            });
        }
        if(nominal==0){
            $(".m_t_nominal").val('');
        }
    });

    $(".jenis").on('change', function(){
        var id = $(this).children("option:selected").val();
        $.ajax({
            url: "{{route('transaksi.getKategori')}}",
            type: "POST",
            data: {
                id: id
            },
            dataType: 'json',
            success: function(respond) {
                var option = '<option value="">-Pilih-</option>';
                respond.forEach(function(key){
                    option += '<option value="'+key.m_k_id+'">'+key.m_k_nama+'</option>';
                });
                $(".m_t_m_k_id").html(option);
            },
            error: function() {
                console.log('Error');
            }
        });
    });
</script>

@endsection
