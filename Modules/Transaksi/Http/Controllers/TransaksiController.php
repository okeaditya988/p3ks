<?php

namespace Modules\Transaksi\Http\Controllers;

use App\Models\Kategori;
use App\Models\Transaksi;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data = new \stdClass();
        $data->i = 1;
        $data->tampil = Transaksi::with('kategori')->whereNull('deleted_at')->get();
        $data->sisaSaldo = DB::table('transaksi')
                                ->select(DB::raw('SUM(IF(m_k_jenis="Pemasukan",m_t_nominal,0))-SUM(IF(m_k_jenis="Pengeluaran",m_t_nominal,0)) as sisaSaldo'))
                                ->rightJoin('kategori','m_t_m_k_id','m_k_id')
                                ->whereNull('transaksi.deleted_at')->first();
        return view('transaksi::index', compact('data'));
    }

    public function tanggal(Request $request)
    {
        $data = Transaksi::with('kategori')
                        ->whereBetween('m_t_tanggal',$request->tanggal)
                        ->whereNull('transaksi.deleted_at')->get();
        return response()->json($data);
    }

    public function get(Request $request)
    {
        $data = new \stdClass();
        $data->tampil = Transaksi::with('kategori')->where('m_t_id',$request->id)->first();
        $data->kategori = Kategori::where('m_k_jenis',($data->tampil)?$data->tampil->kategori->m_k_jenis:'')->get();
        return response()->json($data);
    }


    public function getKategori(Request $request)
    {
        $data = Kategori::where('m_k_jenis',$request->id)->get();
        return response()->json($data);
    }

    public function tambah()
    {
        $data = DB::table('transaksi')
                                ->select(DB::raw('SUM(IF(m_k_jenis="Pemasukan",m_t_nominal,0))-SUM(IF(m_k_jenis="Pengeluaran",m_t_nominal,0)) as sisaSaldo'))
                                ->rightJoin('kategori','m_t_m_k_id','m_k_id')
                                ->whereNull('transaksi.deleted_at')->first();
        return view('transaksi::tambah', compact('data'));
    }

    public function store(Request $request)
    {
        $data = new Transaksi;
        $data->m_t_m_k_id = $request->m_t_m_k_id;
        $data->m_t_tanggal = $request->m_t_tanggal;
        $data->m_t_nominal = $request->m_t_nominal;
        $data->m_t_deskripsi = $request->m_t_deskripsi;
        $data->save();
        if ($data) {
            return redirect()->route('transaksi.index')->with('success', 'Berhasil Ditambahkan');
        }else{
            return redirect()->route('transaksi.index')->with('error', 'Gagal Ditambahkan');
        }
    }

    public function update(Request $request)
    {
        $data = Transaksi::where('m_t_id',$request->m_t_id)
                    ->update([
                        'm_t_m_k_id' => $request->m_t_m_k_id,
                        'm_t_tanggal' => $request->m_t_tanggal,
                        'm_t_nominal' => $request->m_t_nominal,
                        'm_t_deskripsi' => $request->m_t_deskripsi
                    ]);
        if ($data=='1' || $data=='0') {
            return redirect()->route('transaksi.index')->with('success', 'Berhasil Diubah');
        }else{
            return redirect()->route('transaksi.index')->with('error', 'Gagal Diubah');
        }
    }

    public function destroy(Request $request)
    {
        $data = Transaksi::where('m_t_id',$request->id)->delete();
        if ($data=='1' || $data=='0') {
            return response()->json('success');
        }else{
            return response()->json('error');
        }
    }
}
