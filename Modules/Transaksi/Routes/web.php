<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('transaksi')->group(function() {
    Route::get('/', 'TransaksiController@index')->name('transaksi.index');
    Route::post('/tanggal', 'TransaksiController@tanggal')->name('transaksi.tanggal');
    Route::get('/tambah', 'TransaksiController@tambah')->name('transaksi.tambah');
    Route::post('/get', 'TransaksiController@get')->name('transaksi.get');
    Route::post('/getKategori', 'TransaksiController@getKategori')->name('transaksi.getKategori');
    Route::post('/store', 'TransaksiController@store')->name('transaksi.store');
    Route::post('/edit', 'TransaksiController@edit')->name('transaksi.edit');
    Route::post('/update', 'TransaksiController@update')->name('transaksi.update');
    Route::post('/destroy', 'TransaksiController@destroy')->name('transaksi.destroy');
});
