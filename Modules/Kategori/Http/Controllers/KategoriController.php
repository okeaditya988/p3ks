<?php

namespace Modules\Kategori\Http\Controllers;

use App\Models\Kategori;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data = new \stdClass();
        $data->i = 1;
        $data->tampil = Kategori::whereNull('deleted_at')->get();
        return view('kategori::index', compact('data'));
    }

    public function get(Request $request)
    {
        $data = Kategori::where('m_k_id',$request->id)->first();
        return response()->json($data);
    }

    public function tambah()
    {
        return view('kategori::tambah');
    }

    public function store(Request $request)
    {
        $data = new Kategori;
        $data->m_k_nama = $request->m_k_nama;
        $data->m_k_deskripsi = $request->m_k_deskripsi;
        $data->m_k_jenis = $request->m_k_jenis;
        $data->save();
        if ($data) {
            return redirect()->route('kategori.index')->with('success', 'Berhasil Ditambahkan');
        }else{
            return redirect()->route('kategori.index')->with('error', 'Gagal Ditambahkan');
        }
    }

    public function update(Request $request)
    {
        $data = Kategori::where('m_k_id',$request->m_k_id)
                    ->update([
                        'm_k_nama' => $request->m_k_nama,
                        'm_k_deskripsi' => $request->m_k_deskripsi,
                        'm_k_jenis' => $request->m_k_jenis
                    ]);
        if ($data=='1' || $data=='0') {
            return redirect()->route('kategori.index')->with('success', 'Berhasil Diubah');
        }else{
            return redirect()->route('kategori.index')->with('error', 'Gagal Diubah');
        }
    }

    public function destroy(Request $request)
    {
        $data = Kategori::where('m_k_id',$request->id)->delete();
        if ($data=='1' || $data=='0') {
            return response()->json('success');
        }else{
            return response()->json('error');
        }
    }
}
