<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('kategori')->group(function() {
    Route::get('/', 'KategoriController@index')->name('kategori.index');
    Route::get('/tambah', 'KategoriController@tambah')->name('kategori.tambah');
    Route::post('/get', 'KategoriController@get')->name('kategori.get');
    Route::post('/store', 'KategoriController@store')->name('kategori.store');
    Route::post('/edit', 'KategoriController@edit')->name('kategori.edit');
    Route::post('/update', 'KategoriController@update')->name('kategori.update');
    Route::post('/destroy', 'KategoriController@destroy')->name('kategori.destroy');
});
