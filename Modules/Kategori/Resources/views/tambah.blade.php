@extends('layouts.templates')
@section('content')

<div class="block block-themed">
    <div class="block-header block-header-default bg-primary-danger">

        <div class="col-md-10">
            <h3 class="block-title">Tambah Data Kategori</h3>
        </div>
    </div>
    <div class="block-content block-content-full">
        <form class="form-horizontal form" method="post" action="{{route('kategori.store')}}" autocomplete="off">
            {{ csrf_field() }}
            <div class="modal-body row">
                <div class="form-group col-sm-12 row">
                    <label class="col-sm-3 col-form-label">Nama Kategori*</label>
                    <div class="col-sm-9">
                        <input name="m_k_nama" class="form-control m_k_nama" type="text" required>
                    </div>
                </div>
                <div class="form-group col-sm-12 row">
                    <label class="col-sm-3 col-form-label">Jenis Kategori*</label>
                    <div class="col-sm-9">
                        <select name="m_k_jenis" class="form-control m_k_jenis" required>
                            <option value="">-Pilih-</option>
                            <option value="Pemasukan">Pemasukan</option>
                            <option value="Pengeluaran">Pengeluaran</option>
                        </select>
                    </div>
                </div>
                <div class="form-group col-sm-12 row">
                    <label class="col-sm-3 col-form-label">Deskripsi</label>
                    <div class="col-sm-9">
                        <textarea name="m_k_deskripsi" class="form-control m_k_deskripsi"></textarea>
                    </div>
                </div>
                <div class="col-md-12 text-right">
                    <button class="btn btn-lg btn-info simpan">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>

@stop

@section('js')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

@endsection
