@extends('layouts.templates')
@section('content')

<div class="block block-themed">
    <div class="block-header block-header-default bg-primary-danger">

        <div class="col-md-10">
            <h3 class="block-title">Data Kategori</h3>
        </div>
        <div class="col-md-2">
            <a type="button" class="btn btn-success" href="/kategori/tambah">
                <i class="fa fa-plus"></i>
                Tambah
            </a>
        </div>
    </div>
    <div class="block-content block-content-full">
        <table id="dataTable" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama Kategori</th>
                    <th>Jenis Kategori</th>
                    <th>Deskripsi Kategori</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data->tampil as $t)
                <tr>
                    <td>{{$data->i++}}</td>
                    <td>{{$t->m_k_nama}}</td>
                    <td>{{$t->m_k_jenis}}</td>
                    <td>{{$t->m_k_deskripsi}}</td>
                    <td>
                        <a class="btn btn-sm btn-warning button_ubah" onclick="tampil_modal('{{$t->m_k_id}}','ubah')"><i class="fa fa-edit"></i> Ubah</a>
                        <a class="btn btn-sm btn-danger button_hapus" onclick="tampil_modal('{{$t->m_k_id}}','hapus')"><i class="fa fa-trash"></i> Hapus</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade modal_tampil" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="myModalLabel"></h3>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            </div>
            <form class="form-horizontal form" method="post" autocomplete="off">
                {{ csrf_field() }}
                <div class="modal-body row">
                    <input name="m_k_id" class="form-control m_k_id" type="hidden">
                    <div class="form-group col-sm-12 row">
                        <label class="col-sm-3 col-form-label">Nama Kategori*</label>
                        <div class="col-sm-9">
                            <input name="m_k_nama" class="form-control m_k_nama" type="text" required>
                        </div>
                    </div>
                    <div class="form-group col-sm-12 row">
                        <label class="col-sm-3 col-form-label">Jenis Kategori*</label>
                        <div class="col-sm-9">
                            <select name="m_k_jenis" class="form-control m_k_jenis" required>
                                <option value="">-Pilih-</option>
                                <option value="Pemasukan">Pemasukan</option>
                                <option value="Pengeluaran">Pengeluaran</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-sm-12 row">
                        <label class="col-sm-3 col-form-label">Deskripsi</label>
                        <div class="col-sm-9">
                            <textarea name="m_k_deskripsi" class="form-control m_k_deskripsi"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button class="btn btn-info simpan">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

@stop

@section('js')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function tampil_modal(id,jenis){
        if(jenis=='ubah'){
            $("#myModalLabel").html('Ubah Kategori');
            $(".form").attr("action", "{{route('kategori.update')}}");
            $.ajax({
                url: "{{route('kategori.get')}}",
                type: "POST",
                data: {
                    id: id
                },
                dataType: 'json',
                success: function(respond) {
                    $(".m_k_id").val(respond.m_k_id);
                    $(".m_k_nama").val(respond.m_k_nama);
                    $(".m_k_jenis").val(respond.m_k_jenis);
                    $(".m_k_deskripsi").val(respond.m_k_deskripsi);
                },
                error: function() {
                    console.log('Error');
                }
            });
            $(".modal_tampil").modal('show');
        }else{
            tombol_hapus(id,"{{route('kategori.destroy')}}");
        }
    }

</script>

@endsection
