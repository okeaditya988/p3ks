@extends('layouts.templates')
@section('content')

<div class="block block-themed">
    <div class="block-content block-content-full">
        <div class="row gutters-tiny">
            <div class="col-md-12 col-xl-12">
                <a class="block block-transparent" href="javascript:void(0)">
                    <div class="block-content block-content-full bg-gd-leaf text-center">
                        <div class="item item-2x item-circle bg-black-op-10 mx-auto mb-20">
                            <i class="si si-wallet text-white-op"></i>
                        </div>
                        <div class="font-size-h3 font-w600 text-white">Rp. {{number_format((($data)?$data->nominalPemasukan-$data->nominalPengeluaran:'0'),0, ',' , '.')}}</div>
                        <div class="font-size-xl font-w600 text-uppercase text-white-op">Sisa Saldo</div>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-xl-6">
                <a class="block block-transparent" href="javascript:void(0)">
                    <div class="block-content block-content-full bg-gd-lake text-center">
                        <div class="item item-2x item-circle bg-black-op-10 mx-auto mb-20">
                            <i class="si si-wallet text-white-op"></i>
                        </div>
                        <div class="font-size-h3 font-w600 text-white">Rp. {{number_format((($data)?$data->nominalPemasukan:'0'),0, ',' , '.')}}</div>
                        <div class="font-size-xl font-w600 text-uppercase text-white-op">Total Pemasukan</div>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-xl-6">
                <a class="block block-transparent" href="javascript:void(0)">
                    <div class="block-content block-content-full bg-gd-aqua text-center">
                        <div class="item item-2x item-circle bg-black-op-10 mx-auto mb-20">
                            <i class="si si-wallet text-white-op"></i>
                        </div>
                        <div class="font-size-h3 font-w600 text-white">Rp. {{number_format((($data)?$data->nominalPengeluaran:'0'),0, ',' , '.')}}</div>
                        <div class="font-size-xl font-w600 text-uppercase text-white-op">Total Pengeluaran</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>

@stop

@section('js')

@endsection
