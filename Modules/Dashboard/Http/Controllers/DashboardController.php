<?php

namespace Modules\Dashboard\Http\Controllers;

use App\Models\Transaksi;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{

    public function index()
    {
        $data = DB::table('transaksi')
                                ->select(DB::raw('SUM(IF(m_k_jenis="Pemasukan",m_t_nominal,0)) as nominalPemasukan'),DB::raw('SUM(IF(m_k_jenis="Pengeluaran",m_t_nominal,0)) as nominalPengeluaran'))
                                ->rightJoin('kategori','m_t_m_k_id','m_k_id')
                                ->whereNull('transaksi.deleted_at')->first();
        return view('dashboard::index', compact('data'));
    }

}
