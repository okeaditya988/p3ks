<footer id="page-footer" class="opacity-0">
    <div class="content py-20 font-size-xs clearfix">
        <div class="float-right">
            Crafted with <i class="fa fa-heart text-pulse"></i> by <a class="font-w600" href="http://adityahermawan.my.id" target="_blank">Aditya Hermawan</a>
        </div>
        <div class="float-left">
            <a class="font-w600" href="#" target="_blank">Aditya Hermawan</a> &copy; <span class="js-year-copy">2017</span>
        </div>
    </div>
</footer>
