<!-- Codebase Core JS -->
<script src="{{ asset('js/codebase.app.js') }}"></script>
<!-- Page JS Plugins -->
<script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('js/plugins/datatables/buttons/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('js/plugins/datatables/buttons/buttons.html5.min.js') }}"></script>
<script src="{{ asset('js/plugins/datatables/buttons/vfs_fonts.js') }}"></script>
<script src="{{ asset('js/plugins/datatables/buttons/pdfmake.min.js') }}"></script>
<script src="{{ asset('js/plugins/datatables/buttons/jszip.min.js') }}"></script>
<script src="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.js') }}"></script>

<!-- Page JS Code -->
<script src="{{ asset('js/pages/tables_datatables.js') }}"></script>

<script src="{{ asset('js/plugins/sweetalert2/sweetalert2.all.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-notify/bootstrap-notify.min.js') }}"></script>
<script src="{{ asset('js/plugins/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('js/plugins/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{ asset('js/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js')}}"></script>

<script>
    function hanyaAngka(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))

        return false;
        return true;
    }

    @if (session('success'))
        $.notify({
            title: '<strong>Sukses!</strong>',
            message: '{{session("success")}}'
        },{
            type: 'success'
        });
    @endif

    @if (session('error'))
        $.notify({
            title: '<strong>Gagal!</strong>',
            message: '{{session("error")}}'
        },{
            type: 'danger'
        });
    @endif

    function clear(){
        $(":input").not('input[name="_token"]').val('');
        $(".js-select2").val('').trigger('change');
        $(".simpan").show();
        $(".detail").hide();
    }

    $('#dataTable').dataTable( {
        "pageLength": 10
    } );

    function tombol_hapus(id,route,jenis,html){
        swal.fire({
            title: "Hapus?",
            text: "Pastikan Data Akan Dihapus!",
            icon: "warning",
            showCancelButton: !0,
            confirmButtonText: "Ya, Hapus!",
            cancelButtonText: "Tidak, Batalkan!",
            reverseButtons: !0
        }).then(function (e) {
            if (e.value === true) {
                $.ajax({
                    url: route,
                    type: "POST",
                    data: {
                        id: id
                    },
                    dataType: 'json',
                    success: function(respond) {
                        if(respond=='success'){
                            $.notify({
                                title: '<strong>Sukses!</strong>',
                                message: 'Berhasil Dihapus'
                            },{
                                type: 'success'
                            });
                            if(html){
                                html.remove();
                            }
                        }else{
                            $.notify({
                                title: '<strong>Gagal!</strong>',
                                message: 'Gagal Dihapus'
                            },{
                                type: 'danger'
                            });
                        }
                        if(jenis!='detail'){
                            setTimeout(function(){
                                location.reload();
                            }, 500);
                        }
                    },
                    error: function() {
                        console.log('Error');
                    }
                });
            } else {
                e.dismiss;
            }
        }, function (dismiss) {
            return false;
        });
    }

</script>
