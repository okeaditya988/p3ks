<nav id="sidebar">
    <div class="sidebar-content">
        <div class="content-header content-header-fullrow px-15">
            <div class="content-header-section sidebar-mini-visible-b">
                <span class="content-header-item font-w700 font-size-xl float-left animated fadeIn">
                    <span class="text-dual-primary-dark">c</span><span class="text-primary">b</span>
                </span>
            </div>

            <div class="content-header-section text-center align-parent sidebar-mini-hidden">
                <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none align-v-r" data-toggle="layout" data-action="sidebar_close">
                    <i class="fa fa-times text-danger"></i>
                </button>

                <div class="content-header-item">
                    <a class="link-effect font-w700" href="/dashboard">
                        <i class="fa fa-cloud text-primary"></i>
                        <span class=" text-dual-primary-dark">Aplikasi</span><span class="font-size-xl text-primary">P3K</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="content-side content-side-full content-side-user px-10 align-parent">
            <div class="sidebar-mini-visible-b align-v animated fadeIn">
                <img class="img-avatar img-avatar32" src="{{asset('media/avatars/avatar15.jpg')}}" alt="">
            </div>

            <div class="sidebar-mini-hidden-b text-center">
                <a class="img-link" href="be_pages_generic_profile.html">
                    <img class="img-avatar" src="{{asset('media/avatars/avatar15.jpg')}}" alt="">
                </a>
                <ul class="list-inline mt-10">
                    <li class="list-inline-item">
                        <a class="link-effect text-dual-primary-dark font-size-xs font-w600 text-uppercase" href="#">Administrator</a>
                    </li>
                    <li class="list-inline-item">
                        <a class="link-effect text-dual-primary-dark" data-toggle="layout" data-action="sidebar_style_inverse_toggle" href="javascript:void(0)">
                            <i class="si si-drop"></i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a class="link-effect text-dual-primary-dark" href="javascript:void(0)">
                            <i class="si si-logout"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="content-side content-side-full">
            <ul class="nav-main">
                <li>
                    <a class="{{ request()->is('dashboard') ? ' active' : '' }}" href="/dashboard">
                        <i class="si si-cup"></i><span class="sidebar-mini-hide">Dashboard</span>
                    </a>
                </li>
                <li class="nav-main-heading">
                    <span class="sidebar-mini-visible">KT</span><span class="sidebar-mini-hidden">Kategori</span>
                </li>
                <li class="{{ request()->is('kategori/*') ? ' open' : '' }}">
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-layers"></i><span class="sidebar-mini-hide">Kategori</span></a>
                    <ul>
                        <li>
                            <a class="{{ request()->is('kategori') ? ' active' : '' }}" href="/kategori">List Kategori</a>
                        </li>
                        <li>
                            <a class="{{ request()->is('kategori/tambah') ? ' active' : '' }}" href="/kategori/tambah">Tambah Kategori</a>
                        </li>
                    </ul>
                </li>
                <li class="nav-main-heading">
                    <span class="sidebar-mini-visible">TS</span><span class="sidebar-mini-hidden">Transaksi</span>
                </li>
                <li class="{{ request()->is('transaksi/*') ? ' open' : '' }}">
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-note"></i><span class="sidebar-mini-hide">Transaksi</span></a>
                    <ul>
                        <li>
                            <a class="{{ request()->is('transaksi') ? ' active' : '' }}" href="/transaksi">List Transaksi</a>
                        </li>
                        <li>
                            <a class="{{ request()->is('transaksi/tambah') ? ' active' : '' }}" href="/transaksi/tambah">Tambah Transaksi</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
