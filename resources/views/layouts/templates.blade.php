<!doctype html>
<html lang="en" class="no-focus">
    <head>
    @include('layouts.partials.css')
    @yield('css')
 </head>
 <body>
 <div id="app">
 <div id="page-container" class="enable-cookies sidebar-o enable-page-overlay side-scroll page-header-modern side-trans-enabled page-header-fixed">
 @include('layouts.partials.header')
 @include('layouts.partials.menu')
 <main id="main-container">
    <div class="content">
         <div id="contentUpdate">
            @yield('content')
         </div>
    </div>
</main>
@include('layouts.partials.footer')
</div>
</div>
@include('layouts.partials.js')
@yield('js')
</body>
</html>
