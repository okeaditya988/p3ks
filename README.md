Instalasi:

1. Clone repository ke direktori web server.
2. Copy .env.example menjadi .env pada folder project.
3. Buka .env, ganti pengaturan database sesuai dengan web server yang digunakan.
4. Jalankan composer install.
5. Jalankan composer dump-autoload.
6. Jalankan php artisan key:generate
7. Jalankan php artisan config:clear
8. Jalankan php artisan cache:clear
9. Jalankan php artisan migrate --seed
10. Aplikasi siap digunakan
