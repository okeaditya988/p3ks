<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateTransaksiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi', function (Blueprint $table) {
            $table->increments('m_t_id');
            $table->date('m_t_tanggal');
            $table->decimal('m_t_nominal',15,2)->default(0);
            $table->string('m_t_deskripsi',150)->nullable();
            $table->unsignedInteger('m_t_m_k_id');
            $table->foreign('m_t_m_k_id')->references('m_k_id')->on('kategori')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamp('m_t_created_at')->useCurrent();
            $table->timestamp('m_t_updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))->nullable();
            $table->softDeletes('deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi');
    }
}
