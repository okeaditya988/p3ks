<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KategoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['Gaji','Pemasukan'],['Tunjangan','Pemasukan'],['Bonus','Pemasukan'],
            ['Sewa Kost','Pengeluaran'],['Makan','Pengeluaran'],['Pakaian','Pengeluaran'],['Nonton Bioskop','Pengeluaran']
        ];

        foreach ($data as $key => $val) {
            $simpan[$key]= [
                'm_k_nama' => $val[0],
                'm_k_jenis' => $val[1]
            ];
        }
        DB::table('kategori')->insert($simpan);
    }
}
